function addTokens(input, tokens){

    if (typeof input !== 'string') {
        throw new Error('Invalid input'); //nu merge cu Input should be a string
    }    
   
    if (input.length < 6) {
        throw new Error('Input should have at least 6 characters');
    }

    var ok = true;
 
    tokens.forEach((e) => {
     
        if (typeof e !== 'object') {
            ok = false;
        } else {
            var keys = Object.keys(e);
            var values = Object.values(e);
          
            keys.forEach((key) => {
                if (key !== 'tokenName') {
                    ok = false;
                }
            });
         
            if (ok == true) {
                values.forEach((value) => {
                    if (typeof value !== 'string') {
                        ok = false;
                    }
                });              
            }
        }
    });

    if (ok==false) {
        throw new Error('Invalid array format');
    }
    

    
    if (input.indexOf('...') == -1) {
        return input;
    }

    
    tokens.map((e) => {
        input = input.replace('...', '${' + e.tokenName + "}");
    });
    return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;